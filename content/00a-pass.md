Date: 2020-04-29 17:00
Title: Gérer ses mots de passe avec Git et GPG
Slug: gestionnaire-mot-passe-pass
Tags: sécurité
Status: published

On ne présente plus l'intérêt d'un [gestionnaire de
mots-de-passe](https://fr.wikipedia.org/wiki/Gestionnaire_de_mots_de_passe).
C'est une solution élégante pour avoir les avantages de mots de passe
compliqués, de les changer régulièrement sans l'inconvénient de devoir les
retenir. Il y a beaucoup de solutions pour cela, libre comme propriétaire.

Pour ma part, je viens seulement de faire le pas. Une raison qui m'a beaucoup
retenu, c'est le stockage. Je ne veux pas envoyer tous mes mots de passe dans un
site qui sera tôt ou tard compromis. J'ai aussi été déçu d'oublier le mot de
passe maître de mon trousseau de clef
[Révélation](https://packages.debian.org/stretch/revelation), ... (qui n'est
plus maintenu).


# GPG et git

Finalement, un collègue m'a conseillé [password
store](https://www.passwordstore.org/) alias pass. J'ai été conquis par le
principe du projet : stocker les mots de passe dans des fichiers GPG versionnés
avec git. Utilisant déjà GPG et git quotidiennement, cette solution me donne
confiance. Mon mot de passe maître est simplement ma phrase de passe GPG.

La conception du projet est également extrêmement simple. J'apprécie notamment
l'intégration git. La commande `pass git` est juste un passe plat vers la
commande git, mais dans le dossier de stockage des mots de passe. Un simple
`pass git push` et les mots de passes chiffrés sont sauvegardés.

La commande `pass` est déjà très pratique pour naviguer dans ses mots de passe.
C'est aussi simple que de naviguer dans un fichier. La création, le chiffrage,
le versionnage sont très bien intégrés. Le déchiffrage et l'intégration avec le
presse-papier également. Des intégrations avancées simplifient l'utilisation du
trousseau de clef dans les applications graphiques.


# Quelle extension Firefox utiliser ?

Le projet pass est déjà assez populaire et une pléthore d'intégrations existent.
Il y a au moins trois extensions pour Firefox. La plus abouti est
[browserpass](https://addons.mozilla.org/fr/firefox/addon/browserpass-ce/).
Cette extension tire partie de l'API *Native Messaging*, qui gouverne
l'exécution de CLI par les extensions.

Il y a deux logiciels à installer pour former le puzzle : l'extension du
navigateur *browserpass-extension* et un programme CLI
[browserpass-native](https://github.com/browserpass/browserpass-native). Les
versions doivent correspondre. L'extension s'installe normalement depuis le
catalogue officiel de Mozilla. Le programme natif doit être installé depuis une
archive plutôt que par les sources ou le paquet obsolète de votre distribution.
J'ai utilisé la procédure suivante:

``` console
$ cd browserpass-linux64-3.0.6/
$ make BIN=browserpass-linux64 PREFIX=~/.local configure install hosts-firefox-user
/usr/bin/sed -i 's|"path": ".*"|"path": "'"/home/bersace/.local/bin/browserpass-linux64"'"|' browser-files/chromium-host.json
/usr/bin/sed -i 's|"path": ".*"|"path": "'"/home/bersace/.local/bin/browserpass-linux64"'"|' browser-files/firefox-host.json
/usr/bin/install -Dm755 -t "/home/bersace/.local/bin/" browserpass-linux64
/usr/bin/install -Dm644 -t "/home/bersace/.local/lib/browserpass/" Makefile
/usr/bin/install -Dm644 -t "/home/bersace/.local/share/licenses/browserpass/" LICENSE
/usr/bin/install -Dm644 -t "/home/bersace/.local/share/doc/browserpass/" README.md
/usr/bin/install -Dm644 browser-files/chromium-host.json   "/home/bersace/.local/lib/browserpass/hosts/chromium/com.github.browserpass.native.json"
/usr/bin/install -Dm644 browser-files/chromium-policy.json "/home/bersace/.local/lib/browserpass/policies/chromium/com.github.browserpass.native.json"
/usr/bin/install -Dm644 browser-files/firefox-host.json    "/home/bersace/.local/lib/browserpass/hosts/firefox/com.github.browserpass.native.json"
'/home/bersace/.mozilla/native-messaging-hosts/com.github.browserpass.native.json' -> '/home/bersace/.local/lib/browserpass/hosts/firefox/com.github.browserpass.native.json'
$
```

Redémarrage de Firefox et l'extension peut rechercher dans votre trousseau de
clef.

Pour l'heure, il faut renoncer à [Flatpak à cause de *Native
Messaging*](https://bugzilla.mozilla.org/show_bug.cgi?id=1621763).

Un dernier mot sur Firefox Lockwise, le gestionnaire de mot de passe de Firefox.
Je le garde sous le coude pour son historique et surtout, pour stocker les mots
de passe bidon que je ne veux pas sauvegarder dans mon trousseau de clef. Pour
le reste, l'extension browserpass remplace bien l'usage de Firefox Lockwise,
comme préremplir les formulaires.


# Android

C'est un chouille plus compliqué pour Android. Il faut que votre téléphone soit
capable de récupérer un dépôt git et de déchiffrer du GPG. Vous avez besoin de
deux applications :
[OpenKeychain](https://f-droid.org/en/packages/org.sufficientlysecure.keychain)
et [Password Store](https://f-droid.org/en/packages/com.zeapo.pwdstore/).

*Password Store* sait gérer une clef SSH pour cloner le dépôt git. Il suffit
d'ajouter cette clef à votre compte GitLab par exemple. Ensuite, il faut
déchiffrer les messages avec GPG. Pour cela, j'ai importé ma clef privée GPG
dans *OpenKeychain*. *Password Store* vous guidera ensuite pas à pas.

J'ai donc une phrase de passe SSH pour synchroniser les mots de passe et une
phrase de passe GPG pour décoder. L'agent GPG peut garder la phrase de passe un
moment pour rendre la procédure moins fastidieuse. L'expérience utilisateur est
correcte.


# GNOME

GNOME a son trousseau de clef aussi : *GNOME Keyring*. De mon point de vue,
c'est un peu comme Firefox Lockwise : pass fait autorité et le trousseau de clef
de GNOME permet aux différentes applications de stocker les mots de passes que
je renseigne de manière sécurisée, par exemple le mot de passe IMAP dans
Evolution.

Je n'ai pas d'angoisse à perdre mon trousseau de clef GNOME, je n'ai pas besoin
de chercher dedans. D'ailleurs, je ne pense pas que ça ait été vraiment conçu
pour être sauvegardé facilement ni partagé avec son téléphone Android par
exemple.


# En conclusion

Avoir un trousseau de clef pérenne est une brique fondamentale pour améliorer la
sécurité de ses mots de passe. pass fait vraiment bien le travail. Les
intégrations diverses réduisent notablement le surcoût de cette gestion. Essayer
c'est l'adopter !
