Date: 2021-05-19 17:00
Title: JSON5
Slug: json5
Tags: dev
Status: published

Le monde des formats de fichier de configuration est foisonnant. C'est la course
aux standards. ini, xml, json, yaml, toml et py tout court : il y a déjà pas mal
de choix. Et pour combler le tout, je redécouvre [json5](https://json5.org/). La
version 1.0 datant de 2018.


## Fonctionnalités

Les fonctionnalités de JSON5 sont **très** simples :

- Les commentaires sur une ou plusieurs lignes.
- Guillemets optionnels pour les clefs d'objet.
- Virgule finale pour les objets et les tableaux.
- Chaînes de caractères multilignes.
- Guillemets simple ou double pour les chaînes de caractères.
- Les nombres hexadécimaux avec `0xff`.

Et une poignée d'autres extensions que vous trouverez décrites sur la page du
projet. Voici une illustration:

``` yaml
{
  "compatibleJSON": "Rien ne change",
  // commentaire sur une ligne.
  identifiant: 'chaîne avec simples guillemets',
  /*
   * Commentaire sur
   * plusieurs lignes!
   */
  longueValeur: 'ligne 1\
sans \\n\
ligne 2',
  // Les décimaux 0.xx et xx.0 peuvent s'écrire sans 0.
  nombres: [.1234, 1234., -1234, +1234, 0x1234],
  virguleFinale: NaN,
}
```

Autant dire que ça ne fait pas grand chose. Et pourtant, c'est exactement ce qui
manque à JSON pour en faire un bon format de fichier de configuration maintenu
par un humain.


## Comparaisons

D'abord, comme JSON, JSON5 est structuré. On peut encapsuler à l'infini des
valeurs dans des tableaux et des objets. Ce n'est pas le cas du format INI, et
la solution de TOML n'est pas très facile à l'œil humain.

Comme YAML, JSON5 est une extension de JSON. Cela signifie que du JSON est
compatible JSON5, mais pas l'inverse. Mais contrairement à YAML, c'est beaucoup
plus simple et il y a moins de problème de sécurité. En effet, par un système de
marqueur, YAML permet d'exécuter du code arbitraire. Dans vos programmes Python,
utilisez `yaml.safe_load()` de pyyaml ! Dans JSON5, pas de marqueur, tout
simplement. Contrairement à YAML, l'indentation n'a pas de valeurs en JSON5.

Contrairement à XML, c'est un language concis et on n'a pas deux niveaux de
données : éléments et attributs.

Enfin, contrairement à un fichier Python, JSON5 est purement déclaratif, ce qui
faciliter la validation et évite d'implémenter de la logique dans la
configuration.

![XKCD/927](images/xkcd-927-standards.png "Fortunately, the charging one has
been solved now that we've all standardized on mini-USB. Or is it micro-USB?
Shit.")

Le [XKCD #927](https://xkcd.com/927/) réglementaire sur ce sujet.


## Disponibilité

Des bibliothèques existent pour [javascript](https://github.com/json5/json5),
[Python](https://github.com/dpranke/pyjson5),
[Go](https://pkg.go.dev/search?q=json5),
[PHP](https://github.com/colinodell/json5),
[Rust](https://github.com/callum-oakley/json5-rs),
[C](https://github.com/septag/cj5), etc. Les premières implémentations ont plus
de 7 ans.

JSON5 a peu de chances de remplacer JSON. Les analyseurs de JSON sont déjà très
optimisés et ces extensions n'ont pas vraiment d'intérêt pour les échanges entre
services.

Ceci dit, implémenter un analyseur de JSON5 est beaucoup plus simple que YAML,
les performances s'en ressentent également. Bien que la performances de
l'analyse d'un fichier de configuration ne soit pas la partie la plus critique
des logiciels en général.


## Pourquoi 5 ?

On peut se demander pourquoi le chiffre 5 dans JSON5 ? Cela vient du standard
ECMAScript 5 dont sont extraites plusieurs des extensions proposées par JSON5.
Tout simplement. JSON5 est à JSON ce qu'ECMAScript 5 est à Javascript.

Et vous, quel format de configuration privilégiez-vous dans vos projets ? Pour
quelles raisons ?
