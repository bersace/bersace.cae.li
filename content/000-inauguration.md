Title: Inauguration
Date: 2017-11-20 09:00
Slug: inauguration
Status: published

Qui n'a pas senti l'envie d'ouvrir un blog pour finalement se dire : *il y a
tant sur internet, je n'ai rien à dire de plus* ? C'est mon cas, et pourtant
voici mon blog !

En rejoignant Dalibo en mars 2017, j'ai rejoins une entreprise dont l'ADN est
d'être membre actif de la communauté, et même un entraîneur de communauté. J'en
retiens que tout le monde peut apporter quelque chose, pas seulement du code. On
peut être des milliers à programmer, ça ne suffira pas.

Le savoir est comme la musique. Il ne suffit pas que tout le monde achète le
même enregistrement studio. La musique est vivante quand chacun se l'approprie,
l'interprête, la reprend et crée à son tour. C'est possible si les contraintes
légales ne sont pas décourageantes voire totalement contre ce mouvement naturel
de la transmission et de la création du savoir.

![Sonneur](images/sonneur.jpeg)

La toile regorge de savoir, particulièrement sur les sujets qui m'intéressent.
Pourtant je souhaite contribuer par ce blog à rendre ce savoir vivant. Qu'est-ce
que je veux apporter ? À qui ?

Ce blog s'adresse à la communauté francophone des développeurs et techniciens
de l'informatique, mes pairs. On ne peut pas être expert en tout, mais on a
besoin de garder un œil sur ce qui se fait dans le bureau d'à côté, comment les
savoirs se croisent pour accomplir de nouvelles réalisations.

Ma contribution, c'est de partager ce que je fais avec les logiciels libres et
les méthodes agiles pour répondre à nos problèmes d'informaticiens au service
des autres. Ma manière sera de proposer des articles accessibles : courts,
concrets, en Français, en recherchant à être pédagogue.

À toutes fins utiles.
