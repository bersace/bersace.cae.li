Date: 2018-10-09 09:00
Title: .virt
Slug: point-virt
Tags: dns, dnsmasq, envdev, vm, lxc
Status: published

Printemps dernier, j'avais exposé la configuration système pour [résoudre ses
conteneurs sous le domaine `.docker`]({filename}005-dnsdock.md). C'est pratique,
et on voudrait bien ça pour ses conteneurs LXC et ses VM !

L'astuce à la base de la configuration, c'est que *dnsmasq* sait résoudre les
machines auxquelles il distribue une IP. C'est un vrai couteau suisse ! Il
suffit de donner un domaine à dnsmasq et de configurer le hostname des
conteneurs ou des VM. Cet article détaille les étapes pour mettre ça en place.


## lxc-net

Les récents paquets debian de LXC incluent un service *lxc-net* qui se charge de
configurer un pont ethernet et un dnsmasq pour connecter les conteneurs LXC. Le
fichier */etc/default/lxc-net* configure ce service. C'est un simple fichier SH.
On définie le domaine du réseau avec la variable `LXC_DOMAIN`.

``` bash
LXC_DOMAIN=virt
```

Après avoir redémarré le service *lxc-net*, vous pouvez demander au dnsmasq de
`lxcbr0` de résoudre le nom de votre machine. Chez moi, l'adresse de `lxcbr0`
est *192.168.6.254* :

``` console
$ host $(hostname).virt 192.168.6.254
Using domain server:
Name: 192.168.6.254
Address: 192.168.6.254#53
Aliases:

novelo.virt has address 192.168.6.254
$
```

Et voilà ! Bon là, rien de brillant, il affiche sa propre IP. Cela fonctionne
également pour un conteneur LXC :

``` console
$ lxc-start -n pg
$ host pg.virt 192.168.6.254
Using domain server:
Name: 192.168.6.254
Address: 192.168.6.254#53
Aliases:

pg.virt has address 192.168.6.219
$
```

Chouette ! Maintenant, on va rajouter un délégation de zone vers cette IP dans
notre [dnsmasq central]({filename}003-dnsmasq.md).

``` conf
# /etc/dnsmasq.d/lxc.conf
except-interface=lxcbr0
bind-interfaces
# L'IP correspond à celle de lxcbr0.
server=/virt/192.168.6.254
```

Après avoir redémarré le service dnsmasq avec `systemctl restart dnsmasq` par
exemple, tout notre réseau interne sait résoudre le domaine `.virt`. Y compris
nos conteneurs docker !

``` console
$ docker run --rm alpine getent hosts $(hostname).virt
192.168.6.254     novelo.virt  novelo.virt
$
```

On a donc *deux* dnsmasq : un dnsmasq pour connecter les conteneurs LXC et un
dnsmasq central pour aiguiller les requêtes entre le serveur DNS principal
(j'utilise [quad9](https://quad9.net)), le service dnsdock et le dnsmasq LXC.


## Et pour les machines libvirt ?

Seulement à la création d'un réseau, *virt-manager* permet d'assigner un
domaine. Le résultat est le même que pour *lxc-net*. Il faut choisir un domaine
pour le réseau, et aiguiller les requêtes DNS vers le dnsmasq du réseau *virbr*.

Mais je passe par une autre astuce qui évite l'inflation des réseaux et des
domaines : je branche mes VM directement sur le réseau `lxcbr0`. Ainsi j'ai un
unique domaine `.virt` pour mes conteneurs LXC comme pour mes VM. Cela explique
pourquoi j'utilise le domaine `.virt` plutôt que `.lxc` et `.vm` qui sont plus
courant.


## Dotfiles

Je déploie cette configuration avec le rôle ansible
[deimosfr.lxc](https://github.com/deimosfr/ansible-lxc/) de Pierre MAVRO. Le
playbook [lxc.yml](https://gitlab.com/bersace/dotfiles/blob/master/lxc.yml) est
en ligne dans mes dotfiles.

Avec ces astuces, j'ai pas mal allégé la complexité d'usage d'une infrastructure
locale multi-technologies. Je ne cherche plus d'IP, je n'ai plus besoin de fixer
ni de retenir des IP. J'espère que ces astuces vous donnerons des idées pour
votre installation. N'hésitez pas à partager les votres !
