Date: 2023-10-23 17:00
Title: Actualité powerline.bash 2023
Slug: powerline-bash-2023
Tags: bash
Status: published

[powerline.bash] reçoit peu de communication, alors que le projet est bien actif.
powerline.bash est une invite de commande de la famille Powerline écrite en pure bash, en un seul fichier de 2K lignes.
Le projet veille particulièrement à optimiser les performances pour garder votre terminal réactif.

[powerline.bash]: https://gitlab.com/bersace/powerline.bash/

![terminal avec powerline.bash](images/powerline-2023.png)


## Nerd Font v3

Il y a du mouvement du côté des icônes pour terminal.
icons-in-terminal n'a pas reçu de mise-à-jour depuis 2017, on peut le considérer comme abandonné.
Notamment, [Doom Emacs est passé à Nerd Fond](https://github.com/doomemacs/doomemacs/commit/88bb0453887517f144a80fbf60f399176c613d8d).

De son côté, Nerd Font est actif et la v3 apporte pas mal de changements.
D'abord, une rupture de compatibilité.
Si vous utilisez les dernières version de Nerd Font, mettez à jour powerline.bash !
Au passage, Nerd Font a amélioré la qualité des icônes.

De ce fait aujourd'hui, powerline.bash est pensé pour Nerd Fond en premier.


## Ajustement de style

À l'écoute des utilisateurs de powerline.bash, le style a légèrement évoluer.

Un nouveau style d'élision des dossiers longs : `ellipse`.
Par défaut, powerline.bash abbrège les dossiers intermédiaires des chemins profonds.
Avec le paramètre `POWERLINE_PWD_SHORTENING=ellipse`, powerline.bash remplace tous les dossiers intermédiaires par une éllipse : `...`.
Une demande de [@mboy](https://gitlab.com/mboy).

Ou encore :

- l'arobase du segment `hostname` est désormais terni pour mettre en valeur l'utilisateur et le nom d'hôte.
- la commande est désormais mise en gras en préservant la couleur par défaut du terminal, sombre ou clair.


## Nouvelles fonctionnalités

powerline.bash a reçu de nombreuses petites améliorations:

- le nom du virtualenv Python est présenté plutôt que le chemin.
  Très pratique pour les `.venv/`.
  Définissez le nom du venv avec `python -m venv --prompt <nom-venv> .venv/`.
- logo pour raspbian.
- le `$` de l'invite de commande est configurable comme une icône.
- powerline.bash ne dépend plus de la variable `USER`.
- prise en charge de la variable COMPOSE_FILE dans le segment `docker`.

powerline.bash est compatible bash 4.4.12, datant de 2016.
Cela signifie que powerline.bash est compatible avec RHEL8 et suivante ainsi que Debian stretch et suivante.


## Principes

Pour rappel [powerline.bash] se veut :

- libre (Unlicense) ;
- simple à installer et à configurer ;
- performant avec peu de subshell ;
- portable avec peu de dépendances ;
- fonctionnel avec de nombreux segments.

N'hésitez pas à remonter un problème ou partager une idée de fonctionnalité.
Merci à tous ceux qui contribue au succès de ce projet:
par le code, les remontées d'erreur ou les idées et par la communication sur le projet.

**[Commenter cet article sur le Journal du Hacker](https://www.journalduhacker.net/s/kubows/actualit_powerline_bash_2023)**

