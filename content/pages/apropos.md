Title: À propos
Date: 2023-10-25 09:00
Slug: apropos
Status: published

Vous êtes sur le blog d'*Étienne BERSAC*.
Je suis développeur logiciel, principalement en Go sur GNU/Linux.
Mes centres d'intérêt sont l'agilité, la qualité, l'automatisation et les manières de travailler en entreprise.
J'aime concevoir du code utile, libre et collaboratif.

Sur ce blog, je cherche à rédiger du contenu accessible
à destination des techniciens francophones de l'informatique.
On peut voir ça comme le blog d'un compagnon artisan du code.

Je travaille dans la [SCOP](http://www.les-scop.coop/sites/fr/) [Dalibo](https://dalibo.com),
spécialiste de PostgreSQL.


## Mes contributions

En logiciel libre :

- [ldap2pg](https://github.com/dalibo/ldap2pg) chez Dalibo.
- [logfmt](https://gitlab.com/dalibo/logfmt) chez Dalibo.
- [pylogctx](https://github.com/peopledoc/pylogctx) chez [PeopleDoc](https://www.people-doc.fr/)
- [pep440deb](https://gitlab.com/bersace/pep440deb) traduit la version d'un  projet Python en version Debian.
- Notifications graphiques via SSH [libnotify-proxy](https://gitlab.com/bersace/libnotify-proxy)
- [Support des webcam Apple iSight](https://gitlab.com/bersace/isight-firmware-tools)
- [Pilote Linux des ventilateurs d'iMac G5](https://github.com/torvalds/linux/commit/80ff974dba8cc432ab81676fc09d3c357cb11276)

Professionnellement :

- GED de [PeopleDoc](https://www.people-doc.fr/)
- SI dégroupage ADSL chez [Nerim](http://www.nerim.net/)
- SI opérateur mobile virtuel chez Nerim.


## Me contacter

Vous pouvez me contacter par courriel : [etienne.bersac@cae.li](mailto:etienne.bersac@cae.li)

Vous retrouverez mes contributions sur GitLab et GitHub :

- [GitLab/bersace](https://gitlab.com/bersace)
- [GitHub/bersace](https://github.com/bersace)

Ou sur Twitter : [@EtienneBersac](https://twitter.com/EtienneBersac)
