from __future__ import unicode_literals

AUTHOR = 'Étienne BERSAC'
SITENAME = 'bersace.cae.li'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_CATEGORY = 'divers'
DEFAULT_METADATA = {
    'status': 'draft',
}
DEFAULT_PAGINATION = 10

THEME = './themes/attila'
HEADER_COVER = '/images/nuages.jpeg'

AUTHORS_BIO = {
    # cf themes/attila/templates/author.html
    "étienne bersac": {
        "name": "Étienne BERSAC",
        "cover": "images/hackergotchi.png",
        "image": "images/hackergotchi.png",
        "github": "bersace",
        "twitter": "EtienneBersac",
        "location": "France",
        "bio": "Développeur pour éléphants chez Dalibo. Passionné d'agilité. Golfeur de code.",
    }
}

MENUITEMS = [
    ("Dalibo", "https://dalibo.com"),
]

SHOW_CREDITS = {
    'left': 'Édité avec <a href="https://github.com/getpelican/pelican" rel="nofollow">Pelican</a>',  # flake8: noqa
    'right': 'bersace.cae.li',
}

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True
